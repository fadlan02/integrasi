import React from 'react';
import './App.css'
import Header from './Components/Header';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Footer from './Components/Footer';
import DataBuku from './Components/DataBuku';
import FormTambahBuku from './Components/FormTambahBuku';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" exact component={DataBuku} />
          <Route path="/formAddBuku" component={FormTambahBuku} />
        </Switch>

        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;
