import React, { Component, Fragment } from 'react';
import './Header.css';
import { Link } from 'react-router-dom';
class Header extends Component {
    state = {}
    render() {
        return (
            <Fragment>
                <div className="header">
                    <nav>
                        <Link to="/">
                            <h3 className="title-nav">Perpustakaan</h3>
                        </Link>
                        <ul>
                            <Link to="/">
                                <li>Home</li>
                            </Link>
                            <Link to="/pinjamBuku">
                                <li>Pinjam Buku</li>
                            </Link>
                            <Link to="dataPeminjam">
                                <li>Data Peminjam</li>
                            </Link>
                        </ul>
                        <div className="clear"></div>
                    </nav>
                </div>
            </Fragment>
        );
    }
}

export default Header;