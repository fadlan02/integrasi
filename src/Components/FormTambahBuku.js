import React, { Component, Fragment } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';
class FormTambahBuku extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judulBuku: '',
            penerbit: '',
            pengarang: '',
            tahunTerbit: ''
        }
    }
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault()
        console.log(this.state.judulBuku + this.state.idBuku + this.state.pengarang);

        Axios.post(`http://localhost:8484/addBuku`, this.state)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
        alert("Data Berhasil Di tambahkan !!")
    }

    render() {
        return (
            <Fragment>
                <h3 className="title">Form Tambah Buku</h3>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-6">

                            <label>Judul Buku</label>
                            <input type="text" name="judulBuku" onChange={this.handleChange} value={this.state.judulBuku} className="form-control"></input>

                            <label>Pengarang</label>
                            <input type="text" value={this.state.pengarang} onChange={this.handleChange} name="pengarang" className="form-control"></input>

                            <label>Penerbit</label>
                            <input type="text" name="penerbit" value={this.state.penerbit} onChange={this.handleChange} className="form-control"></input>

                            <label>Tahun Terbit</label>
                            <input type="text" onChange={this.handleChange} value={this.state.tahunTerbit} name="tahunTerbit" className="form-control"></input><br />

                            <button type="submit" className="btn btn-success" onClick={this.handleSubmit} style={{ margin: "2px" }}>Simpan</button>
                            <Link to="/">
                                <button className="btn btn-secondary">Kembali</button>
                            </Link>
                        </div>
                    </div>
                </div >
            </Fragment >
        );
    }
}

export default FormTambahBuku;