import React, { Component, Fragment } from 'react';
import './DataBuku.css'
import Axios from 'axios';
import { Link } from 'react-router-dom';
class DataBuku extends Component {
    state = {
        books: [],
        idBuku: '',
        judulBuku: '',
        pengarang: '',
        penerbit: '',
        tahunTerbit: '',
        id: 0
    }

    componentDidMount() {
        Axios.get(`http://localhost:8484/bukus`)
            .then(res => {
                const books = res.data;
                this.setState({
                    books
                });
            })
    }

    handleDelete = (id) => {
        console.log(id);
        Axios.delete('http://localhost:8484/delete/' + id)
            .then(res => {
                const books = res.data;
                this.setState({
                    books
                });
            })
        window.location.reload();
    }

    handleChangeEdit = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
        console.log(this.state.judulBuku + this.state.penerbit + this.state.pengarang + this.state.tahunTerbit);
    }

    tampilEdit = (id) => {
        console.log(id);
        this.setState({
            id: id
        })
    }

    handleEdit = (id) => {
        Axios({
            method: "PUT",
            url: `http://localhost:8484/update/` + id,
            headers: {},
            data: {
                idBuku: this.state.idBuku,
                judulBuku: this.state.judulBuku,
                pengarang: this.state.pengarang,
                penerbit: this.state.penerbit,
                tahunTerbit: this.state.tahunTerbit
            }
        }).then(res => {
            console.log(res);
            console.log(res.data);
            alert("Buku berhasil di update ! ")
        })
    }

    render() {
        return (
            <Fragment>
                <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <label>Judul Buku</label>
                                <input type="text" name="judulBuku" onChange={this.handleChangeEdit} value={this.state.judulBuku} className="form-control"></input>

                                <label>Pengarang</label>
                                <input type="text" name="pengarang" onChange={this.handleChangeEdit} className="form-control" value={this.state.pengarang}></input>

                                <label>Penerbit</label>
                                <input type="text" name="penerbit" value={this.state.penerbit} onChange={this.handleChangeEdit} className="form-control"></input>

                                <label>Tahun Terbit</label>
                                <input type="text" onChange={this.handleChangeEdit} value={this.state.tahunTerbit} name="tahunTerbit" className="form-control"></input><br />


                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <button type="button" className="btn btn-primary" onClick={() => this.handleEdit(this.state.id)}>Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
                <Link to="/formAddBuku">
                    <button className="btn-add">Tambah Buku</button>
                </Link>
                <div className="table table-stripped">
                    <table border="1" cellPadding="20" width="90%" cellSpacing="0">
                        <thead>
                            <tr>
                                <th>ID Buku</th>
                                <th>Judul Buku</th>
                                <th>Pengarang</th>
                                <th>Penerbit</th>
                                <th>Tahun Terbit</th>
                                <th>Setting</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.books.map(books =>
                                    <tr key={books.idBuku}>
                                        <td >{books.idBuku}</td>
                                        <td >{books.judulBuku}</td>
                                        <td >{books.pengarang}</td>
                                        <td >{books.penerbit}</td>
                                        <td >{books.tahunTerbit}</td>
                                        <td ><button className="btn-edit" data-toggle="modal" data-target="#exampleModal" onClick={() => this.tampilEdit(books.idBuku)}><i className="fas fa-edit"></i>Edit</button><button className="btn-hapus" onClick={() => this.handleDelete(books.idBuku)}> <i className="fas fa-trash"></i>Hapus</button></td>

                                    </tr>)
                            }
                        </tbody>
                    </table>
                </div>
            </Fragment>
        );
    }
}

export default DataBuku;